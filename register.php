<?php
require_once "functions.php";
$user = new LoginRegistration();
if($user->getSession()){
  header('Location: index.php');
  exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Registration Page</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="wrapper">
  	<div class="header">
  		<h2>Php Pdo login-registration system</h2>
  	</div>
  	<div class="mainmenu">
  	<ul>
	  		<li><a href="index.php">Home</a></li>
	  		<li><a href="profile.php">Show Profile</a></li>
	  		<li><a href="changePassword.php">Change Password</a></li>
	  		<li><a href="logout.php">Logout</a></li>
	  		<li><a href="login.php">Login</a></li>
	  	    <li><a href="register.php">Register</a></li>
  	</ul>	
  	</div>
  	<div class="content">
  		<h2>Register</h2>
 
  	<p class="msg">
   <?php

   if($_SERVER['REQUEST_METHOD'] == 'POST')
   {
   	    $username 	= $_POST['username'];
   	    $password 	= $_POST['password'];
   	    $name	 	= $_POST['name'];
 	    $email 		= $_POST['email'];

 	    if(empty($username) or empty($password) or empty($name) or empty($email)){
 	    	echo "<span style='color:red'>Error.... Feild must not be empty </span>";
 	    } else {
 	    	$password = md5($password);
 	    	$register = $user->registerUser($username,$password,$name,$email);
 	    	if ($register) {
 	    		echo "<span style='color:green'>Registration Done
 	    		<a href='login.php'>Click here</a>
 	    		For Login</span>";
 	    	}
 	    	else {
 	    		echo "Username or Email already exist";
 	    	}
 	    }
   }

   ?>

  	</p>
  	<div class="login_reg">
  		<form action="" method="post" >
  			<table>
  				<tr>
  					<td>Username:</td>
  					<td><input type="text" name="username" placeholder="please enter your username" /></td>
  				</tr>
  					<tr>
  					<td>Password:</td>
  					<td><input type="password" name="password" placeholder="please give your Password" /></td>
  				</tr>
  					<tr>
  					<td>Name:</td>
  					<td><input type="text" name="name" placeholder="please enter your name" /></td>
  				</tr>
  					<tr>
  					<td>Email:</td>
  					<td><input type="email" name="email" placeholder="please enter your Email" /></td>
  				</tr>
  				<td colspan="2">
  				<span style="float: right">
  					<input type="submit" name="register" value="Register"/>
  					<input type="reset" value="Reset"/>
  					</span>
  				</td>
  			</table>
  		</form>
  	</div>

  	<div class="back" style="text-align: center;margin: 5px; padding: 5px; color:#000";"font-size:20px">
  	<a href="login.php">Back</a>
  	</div>
  	  		</div>
  	<div class="footer">
  		<h2>Mrx Developer</h2>
  	</div>
  </div>
</body>
</html>