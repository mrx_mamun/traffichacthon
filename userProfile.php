<?php
session_start();
require_once "functions.php";
$user = new LoginRegistration();
$uid=$_SESSION['uid'];
$username=$_SESSION['uname'];
if (isset($_REQUEST['id'])) {
	$id=$_REQUEST['id'];
} else {
	header("Location index.php"); 
	exit();
}

if(!$user->getSession()){
  header('Location: index.php');
  exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="wrapper">
  	<div class="header">
  		<h2>Php Pdo login-registration system</h2>
  	</div>
  	<div class="mainmenu">
  	<ul>
      <?php if ($user->getSession()) { ?>

	  		<li><a href="index.php">Home</a></li>
	  		<li><a href="profile.php">Show Profile</a></li>
	  		<li><a href="changePassword.php">Change Password</a></li>
	  		<li><a href="logout.php">Logout</a></li>
          <?php } else { ?>
            
	  		<li><a href="login.php">Login</a></li>
	  	    <li><a href="register.php">Register</a></li>
           <?php }?> 
  	</ul>	
  	</div>
  	<div class="content">
  	<span class="login_msg">
     
  	</span>
  <h2>Welcome <?php echo $username; ?></h2>
 <p class="userlist"> Profile of : <?php $user->getUsername($id); ?></p>
 <table class="tbl_one">
<tr>
	<th>Serial</th>
	<th>Name</th>
	<th>Profile</th>
</tr>
<?php 
$i=0;
$alluser= $user->getAllusers(); 
foreach ($alluser as $user) {
	$i++;

?>

<tr>
	<th><?php echo $i; ?></th>
	<th><?php echo $user['name']; ?></th>
	<th><a href="userProfile.php?id=<?php echo $user['id'];?> "?>View Details</th>
</tr>
<?php } ?>
 </table>
  	  		</div>
  	<div class="footer">
  		<h2>Mrx Developer</h2>
  	</div>
  </div>
</body>
</html>