<?php
require_once "functions.php";
$user = new LoginRegistration();
if($user->getSession()){
  header('Location: index.php');
  exit();
}
?> 
<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="wrapper">
  	<div class="header">
  		<h2>Php Pdo login-registration system</h2>
  	</div>
  	<div class="mainmenu">
  	<ul>
	  		<li><a href="index.php">Home</a></li>
	  		<li><a href="profile.php">Show Profile</a></li>
	  		<li><a href="changePassword.php">Change Password</a></li>
	  		<li><a href="logout.php">Logout</a></li>
	  		<li><a href="login.php">Login</a></li>
	  	    <li><a href="register.php">Register</a></li>
  	</ul>	
  	</div>
  	<div class="content">
  		<h2>Login</h2>
 
  	<p class="msg">
  
   <?php
     if($_SERVER['REQUEST_METHOD'] == 'POST')
     {
      $email = $_POST['email'];
      $password=$_POST['password'];
      if(empty($email) or empty($password))
      {
        echo "<span style='color:#ae23d2'>Feild must not be empty </span>";
      }
      else{
        $password=md5($password);
        $login = $user->loginUser($email,$password);
        if($login)
        {
          header('Location: index.php');
        } else {
          echo "Error... Email or password not match";
        }
      }
     }

   ?>
  	</p>
  	<div class="login_reg">
  		<form action="" method="post" >
  			<table>
  				  <tr>
            <td>Email:</td>
            <td><input type="email" name="email" placeholder="please enter your Email" /></td>
          </tr>
  					<tr>
  					<td>Password:</td>
  					<td><input type="password" name="password" placeholder="please give your Password" /></td>
  				</tr>
  				
  				<td colspan="2">
  				<span style="float: right">
  					<input type="submit" name="login" value="login"/>
  					<input type="reset" value="Reset"/>
  					</span>
  				</td>
  			</table>
  		</form>
  	</div>


  	  		</div>
  	<div class="footer">
  		<h2>Mrx Developer</h2>
  	</div>
  </div>
</body>
</html>